<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes(['register' => false]);

Route::prefix('admin')->name('adm.')->group(function () {
    Route::get('/', 'AdminController@index')->name('panel');

    Route::prefix('blog')->name('blog.')->group(function() {
        Route::get('', 'BlogController@index')->name('index');
        Route::get('create', 'BlogController@create')->name('create');
        Route::post('create', 'BlogController@store')->name('store');
        Route::get('{id}', 'BlogController@show')->name('show');
        Route::post('{id}', 'BlogController@update')->name('update');
        Route::post('{id}/destroy', 'BlogController@destroy')->name('destroy'); 
    });

    Route::prefix('construction')->name('construction.')->group(function() {
        Route::get('', 'ConstructionController@index')->name('index');
        Route::get('create', 'ConstructionController@create')->name('create');
        Route::post('create', 'ConstructionController@store')->name('store');
        Route::get('{id}', 'ConstructionController@show')->name('show');
        Route::post('{id}', 'ConstructionController@update')->name('update');
        Route::post('{id}/destroy', 'ConstructionController@destroy')->name('destroy'); 
    });

    Route::prefix('category')->name('category.')->group(function() {
        Route::get('', 'CategoryController@index')->name('index');
        Route::get('create', 'CategoryController@create')->name('create');
        Route::post('create', 'CategoryController@store')->name('store');
        Route::get('{id}', 'CategoryController@show')->name('show');
        Route::post('{id}', 'CategoryController@update')->name('update');
        Route::post('{id}/destroy', 'CategoryController@destroy')->name('destroy'); 
    });
});
