<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Skeleton</title>

    @toastr_css
</head>

<body>

    <div class="wrapper">

        <!-- Page Content  -->
        <div id="content">
            <h2>Home</h2>
        </div>
    </div>

    @jquery
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
        integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous">
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
        });
    </script>
    @toastr_js
    @toastr_render
</body>

</html>
