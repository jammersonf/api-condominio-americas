<section class="p-0" id="contato">
  <div class="container-fluid">
    <div class="text-center pt-5">
      <div class="center">

        <div class="small-dots">
        </div>
      </div>
      
      <h1 class="text-white display-3">
        Vamos <br>
        <span class="text-orange">Conversar</span>
      </h1>
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          {!! Form::open(['route' => 'site.contact']) !!}
            <div class="form-group">
              <input type="text" class="form-control" name="name" placeholder="Insira seu nome">
            </div>
            <div class="form-group">
              <input type="email" class="form-control" name="email" placeholder="Insira seu e-mail">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="phone" placeholder="Insira seu telefone">
            </div>
            <button class="btn btn-primary btn-lg my-3" type="submit">Enviar</button>
          </form>
    
        </div>
      </div>
      <h1 class="text-white display-3">Social</h1>
      <div class="d-flex justify-content-center">
        <button style="border-radius: 50px" class="btn btn-secondary btn-lg mx-2"><i class="icon icon-facebook icon-2x py-2 px-2"></i></button>
        <button style="border-radius: 50px" class="btn btn-secondary btn-lg mx-2"><i class="icon icon-instagram icon-2x py-2"></i></button>
      </div>

    </div>
    <footer class="bottom-page  ">
      <p><small class="float-left" style="color: #939393;" >Marcos Inácio Advocacia © 2019  |  Política de Privacidade</small></p>
      <img style="right: 0" class="float-right" width="170px" src="{{asset('assets_fronts/imgs/marca_dagua.png')}}" alt="">{{-- marca-dagua.png --}}
    </footer>
  </div>
</section>
