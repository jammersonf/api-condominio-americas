<section class="p-0" id="home">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <!-- Slide One - Set the background image for this slide in the line below -->
            <div class="carousel-item active" style="background-image: url('/assets_fronts/imgs/home_slider_1.png')">
                {{-- home_slider_1 --}}
                <div class="carousel-caption d-none d-md-block">
                </div>
            </div>
            <!-- Slide Two - Set the background image for this slide in the line below -->
            {{-- <div class="carousel-item" style="background-image: url('https://source.unsplash.com/bF2vsubyHcQ/1920x1080')">
                <div class="carousel-caption d-none d-md-block">
                </div>
            </div> --}}
            <!-- Slide Three - Set the background image for this slide in the line below -->
            {{-- <div class="carousel-item" style="background-image: url('https://source.unsplash.com/szFUQoyvrxM/1920x1080')">
                <div class="carousel-caption d-none d-md-block">
                </div>
            </div> --}}
        </div>
        {{-- <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a> --}}
    </div>
    <div class="slider-fade">

        <img width="100%" height="100%" src="{{asset('assets_fronts/imgs/slider_fade.png')}}" alt="">
    </div>
    <div class="home-content tr home-texture">
    </div>

    <div class="home-content tl">
        <h1 class="text-white">
            Segurança nas <br>
            resoluções jurídicas <br>
            entre <span class="text-orange">Portugal e Brasil</span>
        </h1>
        <p class="text-white">Voluptate in est aute cupidatat elit commodo esse qui <br> anim amet ad velit labore
            dolore. Sint irure nisi eiusmod.</p>

        <a href="#contato" class="btn btn-primary btn-lg mt-5 p-4">
            <strong>Entrar em contato</strong>
        </a>
    </div>
    <div class="d-none d-lg-block home-content br">
        <div class="d-flex justify-content-between">

            <div>
                <img style="position: relative; top: 20px; right: 20px; z-index: -10"
                    src="{{asset('assets_fronts/imgs/title_bg.png')}}" alt="">
                <p style="z-index: 10;" class="small-title text-white">
                    Lisboa
                </p>
                <p class="text-white"><small>Sit dolore fugiat commodo sunt <br>
                        sunt cillum ut aliqua ut tempor <br>
                        quis incididunt esse. Enim <br>
                        proident aliqua qui in excepteur. <br>
                        Amet eal.</small></p>
            </div>
            <div>
                <p class="small-title text-white"
                    style="transform: rotate(-90deg); position: relative; bottom: -100px; ">Kristin Richards</p>
            </div>
        </div>

    </div>
</section>