<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('title', 'Título', ['class' => '']) !!}
    {!! Form::text('title', old('title'), ['class' => 'form-control required']) !!}
    @if($errors->has('title'))
      <span class="text-danger">{{ $errors->first('title') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('date', 'Data', ['class' => '']) !!}
    {!! Form::date('date', old('date'), ['class' => 'form-control required ']) !!}
    @if($errors->has('date'))
      <span class="text-danger">{{ $errors->first('date') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('img', 'Imagem', ['class' => '']) !!}<br>
    {!! Form::file('img', old('img'), ['class' => 'form-control']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-4">
    {!! Form::label('text', 'Texto', ['class' => '']) !!}
    {!! Form::textarea('text', old('text'), ['class' => 'form-control required']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-4">
    {!! Form::label('url', 'Url', ['class' => '']) !!}
    {!! Form::text('url', old('url'), ['class' => 'form-control']) !!}
    @if($errors->has('date'))
      <span class="text-danger">{{ $errors->first('date') }}</span>
    @endif
  </div>
</div>


@section('scripts')
  <script src="http://vip.ufcode.com.br/ckeditor/ckeditor.js"></script>
@endsection
