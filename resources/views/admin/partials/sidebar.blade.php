<div class="main-sidebar sidebar-style-2">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand mb-2">
      <a href="{{ route('adm.panel') }}"><img src="{{ asset('/assets/img/brand.svg') }}" width="50"></a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="{{ route('adm.panel') }}"><img src="{{ asset('/assets/img/brand.svg') }}" width="50"></a>
    </div>
    <ul class="sidebar-menu">
      <li class="menu-header">{{ config('app.name') }}</li>
      <li class="menu-header">Módulos</li>
      <li class="dropdown ">
        <a href="#" class="nav-link has-dropdown">
          <i class="far fa-star lga"></i>
          <span>Blog</span>
        </a>
        <ul class="dropdown-menu" style="display: none;">
          <li>
            <a class="nav-link" href="{{ route('adm.blog.index') }}">
                <i class="far fa-star lga"></i>
            <span>Blog</span>
          </a>
          </li>
          <li>
            <a class="nav-link" href="{{ route('adm.category.index') }}">
                <i class="far fa-star lga"></i>
            <span>Categorias</span>
          </a>
          </li>
        </ul>
      </li>
      <li>
        <a href="{{ route('adm.construction.index') }}">
            <i class="far fa-star lga"></i>
        <span>Acompanhamento</span>
      </a>
      </li>
    </ul>
  </aside>
</div>
