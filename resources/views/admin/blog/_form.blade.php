<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('title', 'Título', ['class' => '']) !!}
    {!! Form::text('title', old('title'), ['class' => 'form-control required']) !!}
    @if($errors->has('title'))
      <span class="text-danger">{{ $errors->first('title') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('category', 'Categoria', ['class' => '']) !!}
    {!! Form::select('category_id', $categories, null, ['class' => 'form-control required ']) !!}
    @if($errors->has('category'))
      <span class="text-danger">{{ $errors->first('category') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('img', 'Imagem', ['class' => '']) !!}<br>
    {!! Form::file('img', old('img'), ['class' => 'form-control']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-12">
  <div class="form-group mb-4">
    {!! Form::label('text', 'Texto', ['class' => '']) !!}
    {!! Form::textarea('text', old('text'), ['class' => 'form-control ckeditor required']) !!}
  </div>
</div>


@section('scripts')
  <script src="http://vip.ufcode.com.br/ckeditor/ckeditor.js"></script>
@endsection
