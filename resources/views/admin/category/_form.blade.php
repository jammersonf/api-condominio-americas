<div class="col-sm-12 col-md-12">
  <div class="form-group mb-4">
    {!! Form::label('name', 'Nome', ['class' => '']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control required']) !!}
    @if($errors->has('name'))
      <span class="text-danger">{{ $errors->first('name') }}</span>
    @endif
  </div>
</div>
