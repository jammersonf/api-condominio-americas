<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Construction extends Model
{
    protected $fillable = [
        'title',
        'date',
        'text',
        'img',
        'url',
    ];
}
