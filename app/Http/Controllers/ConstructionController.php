<?php

namespace App\Http\Controllers;

use App\Construction;
use Illuminate\Http\Request;

class ConstructionController extends Controller
{
    protected $construction;

    public function __construct(Construction $c)
    {
        $this->middleware('auth');
        $this->construction = $c;
    }

    public function index()
    {
        $data = $this->construction->orderBy('id', 'desc')->paginate();

        return view('admin.construction.index', compact('data'));
    }

    public function create()
    {
        return view('admin.construction.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $imageName = time().'.'.$input['img']->extension();
        $input['img']->move(public_path('storage/construction'), $imageName);

        $input['img'] = $imageName;
        $input['slug'] = \Str::slug($input['title'], '-');
        $this->construction->create($input);

        toastr()->success('Registro Salvo');

        return redirect()->route('adm.construction.index');
    }

    public function show($id)
    {
        $data = $this->construction->find($id);

        return view('admin.construction.edit', compact('data'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        if (isset($input['img'])) {
            $imageName = time().'.'.$input['img']->extension();
            $input['img']->move(public_path('storage/construction'), $imageName);

            $input['img'] = $imageName;
        } else {
            unset($input['img']);
        }
        
        $this->construction->find($id)->update($input);

        toastr()->success('Registro Atualizado');

        return redirect()->route('adm.construction.index');
    }

    public function destroy($id)
    {
        $this->construction->find($id)->delete();

        toastr()->success('Registro Apagado');

        return redirect()->route('adm.construction.index');
    }
}
