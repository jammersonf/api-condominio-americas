<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    protected $blog;

    public function __construct(Post $post)
    {
        $this->middleware('auth');
        $this->blog = $post;
    }

    public function index()
    {
        $data = $this->blog->orderBy('id', 'desc')->paginate();

        return view('admin.blog.index', compact('data'));
    }

    public function create()
    {
        $categories = Category::pluck('name', 'id')->toArray();
        
        return view('admin.blog.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $imageName = time().'.'.$input['img']->extension();
        $input['img']->move(public_path('storage/posts'), $imageName);

        $input['img'] = $imageName;
        $input['slug'] = \Str::slug($input['title'], '-');
        $this->blog->create($input);

        toastr()->success('Registro Salvo');

        return redirect()->route('adm.blog.index');
    }

    public function show($id)
    {
        $data = $this->blog->find($id);
        $categories = Category::pluck('name', 'id')->toArray();

        return view('admin.blog.edit', compact('data', 'categories'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        if (isset($input['img'])) {
            $imageName = time().'.'.$input['img']->extension();
            $input['img']->move(public_path('storage/posts'), $imageName);

            $input['img'] = $imageName;
        } else {
            unset($input['img']);
        }
        
        $this->blog->find($id)->update($input);

        toastr()->success('Registro Atualizado');

        return redirect()->route('adm.blog.index');
    }

    public function destroy($id)
    {
        $this->blog->find($id)->delete();

        toastr()->success('Registro Apagado');

        return redirect()->route('adm.blog.index');
    }
}
