<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct() {
        $this->middleware('guest');
    }

    public function index(Request $request)
    {
        return view('welcome');
    }

    public function contact(Request $request)
    {
        $messages = Contact::create($request->all()); 

        toastr()->success('Enviado. Logo entraremos em contato!');
        
        return back();
    }
}
