<?php

namespace App\Http\Controllers;

use App\Construction;
use App\Post;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function posts()
    {
        $data = Post::orderBy('id', 'desc')->get();

        return response()->json(['data' => $data], 201);
    }

    public function post($slug)
    {
        $data = Post::whereSlug($slug)->first();

        return response()->json(['data' => $data], 201);
    }

    public function obras()
    {
        $data = Construction::orderBy('id', 'desc')->get();

        return response()->json(['data' => $data], 201);
    }
}
