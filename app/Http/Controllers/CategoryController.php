<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $category;

    public function __construct(Category $post)
    {
        $this->middleware('auth');
        $this->category = $post;
    }

    public function index()
    {
        $data = $this->category->orderBy('id', 'desc')->paginate();

        return view('admin.category.index', compact('data'));
    }

    public function create()
    {
        return view('admin.category.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $this->category->create($input);

        toastr()->success('Registro Salvo');

        return redirect()->route('adm.category.index');
    }

    public function show($id)
    {
        $data = $this->category->find($id);

        return view('admin.category.edit', compact('data'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        
        $this->category->find($id)->update($input);

        toastr()->success('Registro Atualizado');

        return redirect()->route('adm.category.index');
    }

    public function destroy($id)
    {
        $this->category->find($id)->delete();

        toastr()->success('Registro Apagado');

        return redirect()->route('adm.category.index');
    }
}
